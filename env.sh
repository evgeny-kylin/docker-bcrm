#!/usr/bin/env bash

# Update System Packages
apt-get -y upgrade
apt-get install -y unzip git mcrypt curl

#PHP
apt-get -y install php
apt-get -y install php-cli libapache2-mod-php php-fpm php-xml php-zip php-mbstring php-gd php-curl
apt-get -y install php-pgsql
apt-get -y install php-mongodb

#Apache
apt-get -y install apache2
apt-get install -y supervisor

