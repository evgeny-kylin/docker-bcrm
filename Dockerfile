FROM ubuntu:eoan
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive
RUN echo "debconf debconf/frontend select noninteractive" | debconf-set-selections
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
ADD env.sh /env.sh
RUN chmod +x env.sh
ADD project.sh /project.sh
RUN chmod +x project.sh

RUN ./env.sh
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN ./project.sh